package mongo_mocks

import (
	"context"
	"errors"
	"reflect"
	"testing"

	wrappers "gitlab.com/paidit-se/mongo-wrappers"
	"go.mongodb.org/mongo-driver/v2/mongo"
	"go.mongodb.org/mongo-driver/v2/mongo/options"
)

func TestMockClient_Database(t *testing.T) {
	type fields struct {
		Db *MockDatabase
	}
	type args struct {
		name string
		opts []options.Lister[options.DatabaseOptions]
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   wrappers.Database
	}{
		{
			name:   "mock database is returned",
			fields: fields{&MockDatabase{}},
			args:   args{},
			want:   &MockDatabase{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := MockClient{
				Db: tt.fields.Db,
			}
			if got := m.Database(tt.args.name, tt.args.opts...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Database() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMockDatabase_Collection(t *testing.T) {
	type fields struct {
		Coll map[string]*MockCollection
	}
	type args struct {
		name string
		opts []options.Lister[options.CollectionOptions]
	}
	collection := &MockCollection{
		FindFunc: func(ctx context.Context, filter interface{}, opts ...options.Lister[options.FindOptions]) (wrappers.Cursor, error) {
			return nil, errors.New("error")
		},
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   wrappers.Collection
	}{
		{
			name: "mock collection for name is returned",
			fields: fields{Coll: map[string]*MockCollection{
				"one": collection,
				"two": {},
			}},
			args: args{name: "one"},
			want: collection,
		}}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := MockDatabase{
				Coll: tt.fields.Coll,
			}
			if got := m.Collection(tt.args.name, tt.args.opts...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Collection() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMockCollection_DeleteOne(t *testing.T) {
	type fields struct {
		DeleteOneFunc        func(ctx context.Context, filter interface{}, opts ...options.Lister[options.DeleteOneOptions]) (*mongo.DeleteResult, error)
		FindFunc             func(ctx context.Context, filter interface{}, opts ...options.Lister[options.FindOptions]) (wrappers.Cursor, error)
		FindOneAndUpdateFunc func(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.FindOneAndUpdateOptions]) wrappers.SingleResult
		InsertOneFunc        func(ctx context.Context, document interface{}, opts ...options.Lister[options.InsertOneOptions]) (*mongo.InsertOneResult, error)
		UpdateManyFunc       func(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.UpdateManyOptions]) (*mongo.UpdateResult, error)
		UpdateOneFunc        func(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.UpdateOneOptions]) (*mongo.UpdateResult, error)
	}
	type args struct {
		ctx    context.Context
		filter interface{}
		opts   []options.Lister[options.DeleteOneOptions]
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *mongo.DeleteResult
		wantErr bool
	}{
		{
			name: "result from DeleteOneFunc is returned",
			fields: fields{DeleteOneFunc: func(ctx context.Context, filter interface{}, opts ...options.Lister[options.DeleteOneOptions]) (*mongo.DeleteResult, error) {
				return &mongo.DeleteResult{}, nil
			}},
			args:    args{},
			want:    &mongo.DeleteResult{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := MockCollection{
				DeleteOneFunc:        tt.fields.DeleteOneFunc,
				FindFunc:             tt.fields.FindFunc,
				FindOneAndUpdateFunc: tt.fields.FindOneAndUpdateFunc,
				InsertOneFunc:        tt.fields.InsertOneFunc,
				UpdateManyFunc:       tt.fields.UpdateManyFunc,
				UpdateOneFunc:        tt.fields.UpdateOneFunc,
			}
			got, err := m.DeleteOne(tt.args.ctx, tt.args.filter, tt.args.opts...)
			if (err != nil) != tt.wantErr {
				t.Errorf("DeleteOne() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DeleteOne() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMockCollection_Find(t *testing.T) {
	type fields struct {
		FindFunc func(ctx context.Context, filter interface{}, opts ...options.Lister[options.FindOptions]) (wrappers.Cursor, error)
	}
	type args struct {
		ctx    context.Context
		filter interface{}
		opts   []options.Lister[options.FindOptions]
	}
	cursor := &MockCursor{}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    wrappers.Cursor
		wantErr bool
	}{
		{
			name: "mock cursor is returned",
			fields: fields{FindFunc: func(ctx context.Context, filter interface{}, opts ...options.Lister[options.FindOptions]) (wrappers.Cursor, error) {
				return cursor, nil
			}},
			args:    args{},
			want:    cursor,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := MockCollection{
				FindFunc: tt.fields.FindFunc,
			}
			got, err := m.Find(tt.args.ctx, tt.args.filter, tt.args.opts...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Find() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Find() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMockCollection_FindOneAndUpdate(t *testing.T) {
	type fields struct {
		FindOneAndUpdateFunc func(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.FindOneAndUpdateOptions]) wrappers.SingleResult
	}
	type args struct {
		ctx    context.Context
		filter interface{}
		update interface{}
		opts   []options.Lister[options.FindOneAndUpdateOptions]
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *mongo.SingleResult
	}{
		{
			name: "result from FindOneAndUpdateFunc is returned",
			fields: fields{FindOneAndUpdateFunc: func(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.FindOneAndUpdateOptions]) wrappers.SingleResult {
				return &mongo.SingleResult{}
			}},
			args: args{},
			want: &mongo.SingleResult{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := MockCollection{
				FindOneAndUpdateFunc: tt.fields.FindOneAndUpdateFunc,
			}
			if got := m.FindOneAndUpdate(tt.args.ctx, tt.args.filter, tt.args.update, tt.args.opts...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindOneAndUpdate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMockCollection_InsertOne(t *testing.T) {
	type fields struct {
		InsertOneFunc func(ctx context.Context, document interface{}, opts ...options.Lister[options.InsertOneOptions]) (*mongo.InsertOneResult, error)
	}
	type args struct {
		ctx      context.Context
		document interface{}
		opts     []options.Lister[options.InsertOneOptions]
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *mongo.InsertOneResult
		wantErr bool
	}{
		{
			name: "result from FindOneAndUpdateFunc is returned",
			fields: fields{InsertOneFunc: func(ctx context.Context, document interface{}, opts ...options.Lister[options.InsertOneOptions]) (*mongo.InsertOneResult, error) {
				return &mongo.InsertOneResult{}, nil
			}},
			args:    args{},
			want:    &mongo.InsertOneResult{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := MockCollection{
				InsertOneFunc: tt.fields.InsertOneFunc,
			}
			got, err := m.InsertOne(tt.args.ctx, tt.args.document, tt.args.opts...)
			if (err != nil) != tt.wantErr {
				t.Errorf("InsertOne() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InsertOne() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMockCollection_UpdateMany(t *testing.T) {
	type fields struct {
		UpdateManyFunc func(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.UpdateManyOptions]) (*mongo.UpdateResult, error)
	}
	type args struct {
		ctx    context.Context
		filter interface{}
		update interface{}
		opts   []options.Lister[options.UpdateManyOptions]
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *mongo.UpdateResult
		wantErr bool
	}{
		{
			name: "result from UpdateManyFunc is returned",
			fields: fields{UpdateManyFunc: func(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.UpdateManyOptions]) (*mongo.UpdateResult, error) {
				return &mongo.UpdateResult{ModifiedCount: 1, MatchedCount: 1}, nil
			}},
			args:    args{},
			want:    &mongo.UpdateResult{ModifiedCount: 1, MatchedCount: 1},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := MockCollection{
				UpdateManyFunc: tt.fields.UpdateManyFunc,
			}
			got, err := m.UpdateMany(tt.args.ctx, tt.args.filter, tt.args.update, tt.args.opts...)
			if (err != nil) != tt.wantErr {
				t.Errorf("UpdateMany() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateMany() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMockCollection_UpdateOne(t *testing.T) {
	type fields struct {
		UpdateOneFunc func(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.UpdateOneOptions]) (*mongo.UpdateResult, error)
	}
	type args struct {
		ctx    context.Context
		filter interface{}
		update interface{}
		opts   []options.Lister[options.UpdateOneOptions]
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *mongo.UpdateResult
		wantErr bool
	}{
		{
			name: "result from UpdateOneFunc is returned",
			fields: fields{UpdateOneFunc: func(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.UpdateOneOptions]) (*mongo.UpdateResult, error) {
				return &mongo.UpdateResult{ModifiedCount: 1, MatchedCount: 1}, nil
			}},
			args:    args{},
			want:    &mongo.UpdateResult{ModifiedCount: 1, MatchedCount: 1},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := MockCollection{
				UpdateOneFunc: tt.fields.UpdateOneFunc,
			}
			got, err := m.UpdateOne(tt.args.ctx, tt.args.filter, tt.args.update, tt.args.opts...)
			if (err != nil) != tt.wantErr {
				t.Errorf("UpdateOne() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateOne() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMockCursor_Close(t *testing.T) {
	type fields struct {
		closeErr error
		Objs     []string
		curr     int
		err      error
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "error from close is returned",
			fields:  fields{closeErr: errors.New("close error")},
			args:    args{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MockCursor{
				closeErr: tt.fields.closeErr,
				Objs:     tt.fields.Objs,
				curr:     tt.fields.curr,
				Error:    tt.fields.err,
			}
			if err := m.Close(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("Close() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMockCursor_Next(t *testing.T) {
	type fields struct {
		closeErr error
		Objs     []string
		curr     int
		err      error
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "return true if objs remaining",
			fields: fields{
				Objs: []string{"1", "2"},
				curr: 1,
			},
			args: args{},
			want: true,
		},
		{
			name: "return false if no more objs remaining",
			fields: fields{
				Objs: []string{"1"},
				curr: 1,
			},
			args: args{},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MockCursor{
				closeErr: tt.fields.closeErr,
				Objs:     tt.fields.Objs,
				curr:     tt.fields.curr,
				Error:    tt.fields.err,
			}
			if got := m.Next(tt.args.ctx); got != tt.want {
				t.Errorf("Next() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMockCursor_Decode(t *testing.T) {
	type fields struct {
		closeErr error
		Objs     []string
		curr     int
		err      error
	}
	type args struct {
		v interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		want    *TestModel
	}{
		{
			name: "return error if current obj is not valid json",
			fields: fields{
				Objs: []string{"{"},
				curr: 1,
			},
			args:    args{v: &TestModel{}},
			wantErr: true,
		},
		{
			name: "return nil if current obj is valid json",
			fields: fields{
				Objs: []string{`{"a": "test"}`},
				curr: 1,
			},
			args:    args{v: &TestModel{}},
			wantErr: false,
			want:    &TestModel{"test"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MockCursor{
				closeErr: tt.fields.closeErr,
				Objs:     tt.fields.Objs,
				curr:     tt.fields.curr,
				Error:    tt.fields.err,
			}
			if err := m.Decode(tt.args.v); (err != nil) != tt.wantErr {
				t.Errorf("Decode() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.want != nil && !reflect.DeepEqual(tt.want, tt.args.v) {
				t.Errorf("Decode() got = %v, want %v", tt.args.v, tt.want)
			}
		})
	}
}

func TestMockCursor_Err(t *testing.T) {
	type fields struct {
		closeErr error
		Objs     []string
		curr     int
		err      error
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name:    "error is returned",
			fields:  fields{err: errors.New("error")},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MockCursor{
				closeErr: tt.fields.closeErr,
				Objs:     tt.fields.Objs,
				curr:     tt.fields.curr,
				Error:    tt.fields.err,
			}
			if err := m.Err(); (err != nil) != tt.wantErr {
				t.Errorf("Err() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMockSingleResult_Decode(t *testing.T) {
	type fields struct {
		Obj   string
		Error error
	}
	type args struct {
		v interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		want    *TestModel
	}{
		{
			name: "return error if obj is not valid json",
			fields: fields{
				Obj: "{",
			},
			args:    args{v: &TestModel{}},
			wantErr: true,
		},
		{
			name: "return nil if obj is valid json",
			fields: fields{
				Obj: `{"a": "test"}`,
			},
			args:    args{v: &TestModel{}},
			wantErr: false,
			want:    &TestModel{"test"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := MockSingleResult{
				Obj:   tt.fields.Obj,
				Error: tt.fields.Error,
			}
			if err := m.Decode(tt.args.v); (err != nil) != tt.wantErr {
				t.Errorf("Decode() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.want != nil && !reflect.DeepEqual(tt.want, tt.args.v) {
				t.Errorf("Decode() got = %v, want %v", tt.args.v, tt.want)
			}
		})
	}
}

func TestMockSingleResult_Err(t *testing.T) {
	type fields struct {
		Obj   string
		Error error
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name:    "error is returned",
			fields:  fields{Error: errors.New("error")},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := MockSingleResult{
				Obj:   tt.fields.Obj,
				Error: tt.fields.Error,
			}
			if err := m.Err(); (err != nil) != tt.wantErr {
				t.Errorf("Err() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

type TestModel struct {
	A string
}
