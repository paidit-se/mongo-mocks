package mongo_mocks

import (
	"context"
	"encoding/json"

	wrappers "gitlab.com/paidit-se/mongo-wrappers"
	"go.mongodb.org/mongo-driver/v2/mongo"
	"go.mongodb.org/mongo-driver/v2/mongo/options"
)

type MockClient struct {
	Db *MockDatabase
}

func (m MockClient) Database(name string, opts ...options.Lister[options.DatabaseOptions]) wrappers.Database {
	return m.Db
}

var _ wrappers.Client = &MockClient{}

type MockDatabase struct {
	Coll map[string]*MockCollection
}

func (m MockDatabase) Collection(name string, opts ...options.Lister[options.CollectionOptions]) wrappers.Collection {
	return m.Coll[name]
}

var _ wrappers.Database = &MockDatabase{}

type MockCollection struct {
	DeleteOneFunc        func(ctx context.Context, filter interface{}, opts ...options.Lister[options.DeleteOneOptions]) (*mongo.DeleteResult, error)
	FindFunc             func(ctx context.Context, filter interface{}, opts ...options.Lister[options.FindOptions]) (wrappers.Cursor, error)
	FindOneAndUpdateFunc func(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.FindOneAndUpdateOptions]) wrappers.SingleResult
	InsertOneFunc        func(ctx context.Context, document interface{}, opts ...options.Lister[options.InsertOneOptions]) (*mongo.InsertOneResult, error)
	UpdateManyFunc       func(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.UpdateManyOptions]) (*mongo.UpdateResult, error)
	UpdateOneFunc        func(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.UpdateOneOptions]) (*mongo.UpdateResult, error)
}

func (m MockCollection) DeleteOne(ctx context.Context, filter interface{}, opts ...options.Lister[options.DeleteOneOptions]) (*mongo.DeleteResult, error) {
	return m.DeleteOneFunc(ctx, filter, opts...)
}

func (m MockCollection) Find(ctx context.Context, filter interface{}, opts ...options.Lister[options.FindOptions]) (wrappers.Cursor, error) {
	return m.FindFunc(ctx, filter, opts...)
}

func (m MockCollection) FindOneAndUpdate(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.FindOneAndUpdateOptions]) wrappers.SingleResult {
	return m.FindOneAndUpdateFunc(ctx, filter, update, opts...)
}

func (m MockCollection) InsertOne(ctx context.Context, document interface{}, opts ...options.Lister[options.InsertOneOptions]) (*mongo.InsertOneResult, error) {
	return m.InsertOneFunc(ctx, document, opts...)
}

func (m MockCollection) UpdateMany(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.UpdateManyOptions]) (*mongo.UpdateResult, error) {
	return m.UpdateManyFunc(ctx, filter, update, opts...)
}

func (m MockCollection) UpdateOne(ctx context.Context, filter interface{}, update interface{}, opts ...options.Lister[options.UpdateOneOptions]) (*mongo.UpdateResult, error) {
	return m.UpdateOneFunc(ctx, filter, update, opts...)
}

var _ wrappers.Collection = &MockCollection{}

type MockCursor struct {
	closeErr error
	Objs     []string
	curr     int
	Error    error
}

func (m *MockCursor) Close(ctx context.Context) error {
	return m.closeErr
}

func (m *MockCursor) Next(ctx context.Context) bool {
	if len(m.Objs) > m.curr {
		m.curr += 1
		return true
	}
	return false
}

func (m *MockCursor) Decode(v interface{}) error {
	return json.Unmarshal([]byte(m.Objs[m.curr-1]), v)
}

func (m *MockCursor) Err() error {
	return m.Error
}

var _ wrappers.Cursor = &MockCursor{}

type MockSingleResult struct {
	Obj   string
	Error error
}

func (m MockSingleResult) Decode(v interface{}) error {
	return json.Unmarshal([]byte(m.Obj), v)
}

func (m MockSingleResult) Err() error {
	return m.Error
}

var _ wrappers.SingleResult = &MockSingleResult{}
