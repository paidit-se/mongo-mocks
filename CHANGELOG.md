# Changelog

All notable changes to this project will be documented in this file.

## [0.0.7] - 2025-03-03

### 🐛 Bug Fixes

- *(deps)* Update module go.mongodb.org/mongo-driver/v2 to v2.1.0
- *(deps)* Update module gitlab.com/paidit-se/mongo-wrappers to v0.0.8

## [0.0.6] - 2025-01-01

### 🐛 Bug Fixes

- *(deps)* Update module go.mongodb.org/mongo-driver to v2

## [0.0.5] - 2024-10-06

### 🐛 Bug Fixes

- *(deps)* Update module gitlab.com/paidit-se/mongo-wrappers to v0.0.6

## [0.0.4] - 2024-10-05

### 🐛 Bug Fixes

- *(deps)* Update module go.mongodb.org/mongo-driver to v1.15.0
- *(deps)* Update module go.mongodb.org/mongo-driver to v1.15.1
- *(deps)* Update module go.mongodb.org/mongo-driver to v1.16.0
- *(deps)* Update module go.mongodb.org/mongo-driver to v1.16.1
- *(deps)* Update module go.mongodb.org/mongo-driver to v1.17.0
- *(deps)* Update module go.mongodb.org/mongo-driver to v1.17.1

### 💼 Other

- Remove Docker from dependabot config

### ⚙️ Miscellaneous Tasks

- Add dependabot config
- Change to codecov binary instead of bash uploader
- Cleanup imports
- Add params to codecov
- Switch to manual rebases for Dependabot
- Add release flow

## [0.0.3] - 2021-02-26

### ⚙️ Miscellaneous Tasks

- Updated to latest version of mongo-wrappers

## [0.0.2] - 2020-07-03

### 🚀 Features

- Mock more methods

## [0.0.1] - 2020-06-25

### 🚀 Features

- Initial commit

### 🐛 Bug Fixes

- Add GOPRIVATE and git config since mongo-wrappers is private

<!-- generated by git-cliff -->
