# mongo-mocks

[![Build Status](https://gitlab.com/paidit-se/mongo-mocks/badges/master/pipeline.svg)](https://gitlab.com/paidit-se/mongo-mocks/commits/master)
[![codecov](https://codecov.io/gl/paidit-se/mongo-mocks/branch/master/graph/badge.svg?token=kiSaqwUdxr)](https://codecov.io/gl/paidit-se/mongo-mocks)

# What is mongo-mocks?
It's a small library of mock-implementations for the [mongo-wrappers](https://gitlab.com/paidit-se/mongo-wrappers).

# How to use
See the [closer](https://gitlab.com/paidit-se/closer) project for an example
